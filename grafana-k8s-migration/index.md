[[_TOC_]]

# Grafana dashboards in Kubernetes

## Overview

We currently run a private instance of Grafana at
[dashboards.gitlab.net](https://dashboards.gitlab.net/) to build dashboards
visualizing our metrics from Prometheus, Elasticsearch and various other data
sources.

This document is a readiness review for the migration of Grafana to Kubernetes and
improvements to its configuration management.

## Problem

Our Grafana instance is currently hosted on a single GCE VM, backed by a local
SQLite database, and with a GCE load-balancer in front, and that's about it.

```plantuml
!theme materia
component "GCE load-balancer" as LB
node "dashboards-01-inf-ops" as VM {
  component "Grafana" as APP
  database "SQLite" as DB
}

LB --> APP
APP --> DB
```

There are several problems with this setup:

- contrary to
  [our claim in the handbook](https://about.gitlab.com/handbook/engineering/monitoring/#private-monitoring-infrastructure),
  the service is not highly available: there is no redundancy, no failover, and
  it doesn't scale at all, as it's a single VM with no autoscaling;
- the maintenance of this service has been low-priority, as such to this day we
  are still running Grafana v7.2.0 (2020-09-23) while the latest version as of
  the time of writing is v8.2.1;
- the SQLite database makes HA impossible and is never backed up (aside from
  the VM snapshots);
- the Grafana plugins have been installed manually, offering little visibility
  and control over them, they are rarely updated, some could become vulnerable
  eventually;
- the [Grafana image renderer](https://github.com/grafana/grafana-image-renderer)
  in particular is running locally, and can be a security risk as it spawns a
  headless Chromium instance to take screenshots of dashboard panels;
- the Grafana datasources are setup manually, offering little visibility and
  control over them, and a number of them are no longer functional and trying
  to use them can result in errors from Grafana.

## Proposed solution

```plantuml
!theme materia
cloud "Cloudflare" as CF
rectangle "GCP" {
    component "GCE load-balancer" as LB
    rectangle "ops-gitlab-gke k8s cluster" {
        rectangle "monitoring namespace" {
            rectangle "grafana pod" {
              component "Grafana" as APP
              component "CloudSQL Proxy" as CP
            }
            rectangle "grafana-image-renderer pod" {
              component "Grafana Image Render plugin" as IM
            }
        }
    }
    database "CloudSQL PostgreSQL" as DB
}

CF --> LB
LB --> APP
APP --> CP
CP --> DB
APP <--> IM
```

### Kubernetes deployment

Grafana is deployed to the 'ops-gitlab-gke' GKE cluster from the
[`tanka-deployment` repository](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/tanka-deployments).

#### Grafana application

The application is deployed in pods running:
- [the official Grafana docker container](https://hub.docker.com/r/grafana/grafana)
- the [CloudSQL Auth Proxy](https://cloud.google.com/sql/docs/postgres/connect-kubernetes-engine)
  as a sidecar container, which is the method recommended by Google to connect
  to a CloudSQL instance from GKE, as it provides strong encryption and
  authentication using IAM.

The HTTP service is exposed on the default port 3000, and Prometheus
metrics are gathered from the endpoint `/metrics` on that same port.

We leverage [Horizontal Pod Autoscaling](https://kubernetes.io/docs/tasks/run-application/horizontal-pod-autoscale/)
to autoscale depending on demand. At the time of writing, we are targeting to
always stay below
[60% CPU and 60% memory usage](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/tanka-deployments/-/blob/b31781c895bd3b908ae6ff965cf9c3be7c3944d9/lib/gl-grafana/grafana.libsonnet#L198-207).

Grafana is a fairly light service, so this should be plenty of capacity.

#### Grafana Image Renderer plugin

The [Grafana Image Renderer plugin](https://github.com/grafana/grafana-image-renderer)
is a security concern because it spawns headless Chromium instances to be able
to take dashboard panel screenshots. For that reason, it is deployed in
separate pods running
[the official Grafana Image Renderer plugin docker container](https://hub.docker.com/r/grafana/grafana-image-renderer).

The HTTP service is exposed on the default port 8081, and Prometheus
metrics are gathered from the endpoint `/metrics` on that same port.

We leverage [Horizontal Pod Autoscaling](https://kubernetes.io/docs/tasks/run-application/horizontal-pod-autoscale/)
to autoscale depending on demand. At the time of writing, we are targeting to
always stay below
[60% CPU and 60% memory usage](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/tanka-deployments/-/blob/b31781c895bd3b908ae6ff965cf9c3be7c3944d9/lib/gl-grafana/grafana.libsonnet#L398-407).

Given that the plugin spawns headless Chromium instances, it can be heavy on CPU
and memory usage. It is
[configured in clustered mode](https://grafana.com/docs/grafana/latest/image-rendering/#clustered)
to optimize its performances. At the time of writing,
[the max concurrency per instance is `5`](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/tanka-deployments/-/blob/b31781c895bd3b908ae6ff965cf9c3be7c3944d9/lib/gl-grafana/grafana.libsonnet#L88).

Those pods have ingress and egress policies restricting traffic to and from the
Grafana pods only, so that those Chromium instances are not exposed to the rest
of the cluster and are not able to reach anything outside of Grafana in case it
ever gets compromised.

### Database

Grafana connects to a Cloud SQL PostgreSQL database hosted on a single instance with 1
CPU and 4GiB of memory, and a 10GiB disk. Automated backups are enabled. It is
provisioned via Terraform, along with the Google Service Account used to access
it.

Grafana is not particularly heavy on its database, the schema is fairly simple
and it doesn't execute complex queries. Also the current SQLite database is
only about 500MiB which would largely fit in memory for this instance. It is
very unlikely to become a bottleneck.

Grafana connects to the database via
[CloudSQL Auth Proxy](https://cloud.google.com/sql/docs/postgres/connect-kubernetes-engine)
in a sidecar container, which is the method recommended by Google, as it
provides strong encryption and authentication using IAM.

### Ingress

Grafana has a GCE ingress configured in Kubernetes, with a GCE-managed TLS
certificate and a security policy set to *RESTRICTED*, which allows only TLS
v1.2+ with strong moderns ciphers.
[Container-native load balancing](https://cloud.google.com/kubernetes-engine/docs/concepts/container-native-load-balancing)
is explicitely enabled for improved network performances, also enabling
advanced features such as Cloud Armor or IAP if we ever need them in the
future.

The Cloudflare proxy is enabled on `dashboards.gitlab.net` to benefit from the
WAF and DDoS mitigation features.

### Security

The only external resource accessible by the Grafana pods is its CloudSQL
instance. The Google Service Account associated to its Kubernetes Service
Account only has the role `roles/cloudsql.client` and is not able to access any
other resource in the GCP project.

See also [the above section](#grafana-image-renderer-plugin) for security
measures around the Grafana Image Renderer plugin.

### Authentication

Authentication is configured with Google OAuth restricted to the `gitlab.com`
domain, allowing access only to GitLab team members. The OAuth Client ID is
created manually in the `gitlab-ops` project, and the Client ID and Secret are
configured from the CI variables to be stored in a Kubernetes Secret.

Only the default `admin` user is able to login via Basic authentication, and
anonymous access is disabled.

### Plugins provisioning

The Grafana plugins to be installed are listed in the Tanka deployment configuration
and [installed on pod startup from the variable `GF_INSTALL_PLUGINS`](https://grafana.com/docs/grafana/latest/installation/docker/#install-plugins-in-the-docker-container).
If not explicitely set the latest version will be installed. In normal
circumstances this takes about 4 seconds in total for the 8 plugins currently
configured.

*Note:* We should consider a way to cache those installed plugins to avoid
reinstalling them on each pod startup, but that would require either some
persistent storage or building a custom image with preinstalled plugins.
Failure to install a plugin results in the Grafana container crashing on
startup, which could impact autoscaling if for instance `grafana.com` is
unavailable.

### Datasources provisioning

The datasources formerly configured manually have been removed to be
reprovisioned from Tanka deployment configuration, which sets them in
ConfigMaps mounted in the Grafana container to be
[provisioned on startup](https://grafana.com/docs/grafana/latest/administration/provisioning/#data-sources).

```
# values.jsonnet
---
{
  ops: {
    datasources+: {
      prometheus_dev_gitlab_org: {
        name: 'dev.gitlab.org',
        type: 'prometheus',
        url: 'http://dev.gitlab.org:9090',
        jsonData: {
          httpMethod: 'GET',
          timeInterval: '5s',
        },
      },
    },
  },
}
```

If the datasource requires credentials, they can be set from the environment
and can be safely stored in Secrets like so:
```
{
  ops: {
    datasources+: {
      sitespeed: {
        name: 'sitespeed',
        type: 'graphite',
        url: 'http://1.2.3.4:8080',
        basicAuth: {
          user: '${DS_SITESPEED_USER}',
          password: '${DS_SITESPEED_PASSWORD}',
        },
        jsonData: {
          graphiteVersion: '1.1',
        },
      },
    },
    datasource_secrets+: {
      sitespeed_user: {
        env: 'DS_SITESPEED_USER',
        value: std.extVar('ds_sitespeed_user'),
      },
      sitespeed_password: {
        env: 'DS_SITESPEED_PASSWORD',
        value: std.extVar('ds_sitespeed_password'),
      },
    },
  },
}
```

Datasources provisioned from files are then stored in the database, but
deleting the provisioning file is not enough and deleting them from the UI is
not possible either. To do so instead, its configured must be updated with
`delete: true` so that Tanka adds it to a provisioning file that will instruct
Grafana to delete it from the database on startup:
```
# values.jsonnet
---
{
  ops: {
    datasources+: {
      prometheus_dev_gitlab_org: {
        name: 'dev.gitlab.org',
        delete: true,
      },
    },
  },
}
```

*Note:* initially for this migration, only the currently functional and in-use
datasources are configured:
- most Prometheus datasources are present, except those that are no longer
  present or accessible;
- all Elasticsearch datasources are removed because of incorrect credentials
  and/or URLs, but can be re-added easily as they are simply commented out in
  the configuration;
- one Sitespeed datasource is removed because it is timing out and not
  referenced in any dashboard, the other one is renamed from `sitespeed new` to
  `sitespeed` and the few dashboards using it will need to be updated
  accordingly;
- the Google BigQuery, Pagerduty, Prometheus AlertManager and Simple
  Annotations datasources are removed because they are not referenced in any
  dashboard.

### Dashboards provisioning

Like with datasources,
[dashboards can be provisioned from files](https://grafana.com/docs/grafana/latest/administration/provisioning/#dashboards)
which can be stored in ConfigMaps, provided that they are small enough to not
go past the 1MiB limit from `etcd`. Unlike datasources, those dashboards are
never stored in the database unless modified (which can be disabled), and
dashboards from files always override those from the database for matching
UIDs. This can make the deployment of our
[auto-generated dashboards](https://gitlab.com/gitlab-com/runbooks/-/tree/master/dashboards)
much easier as there would be no synchronization or cleanup needed, with no
risk to interfere with manually created dashboards (as long as the UIds don't
conflict).

Unfortunately, because of the way old versions of `kubectl` store the ConfigMap
content into annotations that are limited to 256KiB for diff purposes, some of
our largest dashboards cannot be deployed with this method until the GKE
cluster is upgraded to v1.22. So for now, dashboards are still deployed with a
synchronization script using the API.

### Monitoring

Both Grafana and the Grafana Image Renderer plugin expose Prometheus metrics on
the endpoint `/metrics`. The metrics catalog for `monitoring` and the
[monitoring](https://dashboards.gitlab.net/d/monitoring-main/monitoring-overview)
dashboard is updated to display metrics from the Google load-balancer and the
`grafana` and `grafana-image-renderer` pods.
