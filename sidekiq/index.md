# Sidekiq  Migration to Kubernetes

Sidekiq is a service that monitors a queue for work.  When a queue has a job
provided to it, Sidekiq then spins up an appropriate rails controller to process
the job.  Various items place work into the queue which include GitLab cron jobs
and user behavior on GitLab. GitLab uses Redis as the queue.  Queues are divided
amongst a large fleet of servers which each contain a specific configuration.
This configuration includes which queues that set of servers will listen, how
many workers Sidekiq operates, and the concurrency level for that worker.  Our
goal is to move our Sidekiq fleet of servers into Kubernetes to make management
of Sidekiq easier and to offer a more robust method of operating the various
types of work that Sidekiq performs.

This readiness review covers the Sidekiq service as a whole.  Please note that
currently we are attempting to migrate specific queues at a time.  Refer to the
appropriate readiness review document for each queue here: [queue specific file]

The Operational Runbook for this service can be found in our runbooks project:
https://gitlab.com/gitlab-com/runbooks/blob/master/troubleshooting/service-sidekiq.md

## Table of contents

  * [Architecture overview](#architecture-and-overview)
  * [Configuration](#configuration)
  * [Risk assessment](#risk-assessment-and-blast-radius-of-failures)
  * [Security considerations](#security-considerations)
  * [Application upgrade/rollback](#application-upgrade-and-rollback)
  * [Observability and Monitoring/Logging](#observability-and-monitoring)
  * [Testing](#testing)

## Architecture and Overview

Sidekiq is a partially stateless service that is leveraged by GitLab to perform
work to be done asynchronously with user interaction on the application, or
process cron jobs that perform a wide array of tasks in the background to.

It's noted to be partially stateless as there's on-going work to remove the need
to have a shared file system mounted to our fleet of Sidekiq servers.  Not all
queues require a shared filesystem amongst our infrastructure.  Necessary issues
will be opened as those are found.  Any queue that relies on shared storage will
not be migrated to Kubernetes until a later time.

### Operational Diagram

```mermaid
sequenceDiagram
  participant J as job/cronjob
  participant R as redis
  participant S as Sidekiq

  J ->> R: insert job

  activate R
    S ->> R: monitor queue
    S ->> R: remove from queue
  deactivate S

  loop work
    S ->> S: processing
  end
```

## Configuration

Configurations of Sidekiq are stored in two locations.  For omnibus
installations, all required configurations and secrets will be stored in our
existing [chef repo] or [gkms vaults].  For Kubernetes, we will leverage the
helm chart configuration, located in [k8s-workloads/gitlab-com] and secret
objects that are manually created in the Kubernetes clusters. Secrets identified
and utilized by any component introduced into Kubernetes, is documented here:
https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/blob/4d3cbe754e00e1eb3ae6b53deb93d9261a2d7109/HELM_README.md

Our chef configurations are complex enough we utilize a tool to automatically
build the chef roles:
https://ops.gitlab.net/gitlab-cookbooks/chef-repo/tree/master/tools/sidekiq-config
A similar tool has not yet been created for the Kubernetes installation method.
Due to the nature of how the configuration is defined in our helm chart, this
tool may not be necessary.  We can evaluate this at a later time if desired. The
helm configuration for each queue is held under the attribute
`gitlab.sidekiq.pods` as seen here:
https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/blob/4d3cbe754e00e1eb3ae6b53deb93d9261a2d7109/values.yaml#L524-542
This notation is documented here:
https://docs.gitlab.com/charts/charts/gitlab/Sidekiq/#per-pod-settings

An audit was performed when pushing a single queue into the `pre` environment:
https://gitlab.com/gitlab-com/gl-infra/delivery/issues/591 A similar audit may
need to be performed when a queue is initially added to other environments as
well.  We currently do not have a method for which we can centrally manage both
omnibus and helm configurations and secrets from one central location. This
introduces risk that a change made to omnibus may be forgotten which may impact
our Sidekiq deployments.

An issue exists to start the conversation to resolve the above situation:
https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/8888

Please refer to the [queue specific file] to refer to any additional information
related to the Sidekiq configuration.

### Canary

Sidekiq does not currently support the ability to segregate workloads between
environments.  Due to this we will not have a canary specific configuration for
Sidekiq in Kubernetes.  This is on par with our current omnibus installation for
GitLab.com

## Risk Assessment and Blast radius of Sidekiq

Sidekiq is a service that runs multiple instances for the purposes of handling
load.  Our current design utilizes multiple sets of fleets to spread specific
jobs to specific sets of servers depending on a variety of factors.  These
include, but are not limited too:

* priority - should it be done ASAP, or can this wait a few minutes
* load - is the job something that would take a lot of resources to complete

Our helm charts provides us with the capability to be as granular as we desire.
We could create a configuration that mimics our current VM configuration, we
could setup a Kubernetes Deployment per queue, or something in between. The
style for which we'll move queues over into Kubernetes has yet to be fully
planned.  See the [queue specific file] for further details as we progress this
work.

### Reliable Fetcher

Sidekiq itself uses a [reliable fetcher] in order to prevent jobs dangling
should there be a catastrophic failure of a Pod while processing jobs.  If
Sidekiq is signaled by Kubernetes during a deploy, or if a Pod is deemed
unhealthy, the correct signal is sent to the Sidekiq process to ensure any
active jobs are placed back onto the queue to be tried again by another Sidekiq
Pod.

[Reference](https://gitlab.com/gitlab-org/charts/gitlab/issues/1758#note_262503350)

The consequences of a service degradation vary greatly depending on the queue.
See the [queue specific file] for further details.

## Resource Management

See the [queue specific file] for details.

## Security Considerations

### Network Access

See the [queue specific file] for details.

### Abuse

See the [queue specific file] for details.

## Application Upgrade and Rollback

```mermaid
sequenceDiagram
    participant Administrator
    participant GitLab.com
    Administrator->>GitLab.com: Submits an MR for review
    loop CI
        GitLab.com->>GitLab.com: Syntax checks
    end
    GitLab.com->>ops.gitlab.net: branch mirrored to ops.gitlab.net
    loop CI
        ops.gitlab.net->>ops.gitlab.net: Dryrun apply on all envs
    end
    Administrator->>GitLab.com: MR merged to master
    loop CI
        GitLab.com->>GitLab.com: Syntax checks
    end
    GitLab.com->>ops.gitlab.net: master mirrored to ops.gitlab.net
    loop CI
        ops.gitlab.net->>ops.gitlab.net: Applied to non-production envs
    end
    loop CI
        ops.gitlab.net->>ops.gitlab.net: Applied to production
    end
```

These versions are set for the Sidekiq service:

* `CHART_VERSION` which is set in the [configuration project](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/blob/18a3efa89bce59bf2f3c6d28d37cdcd9ade7164f/CHART_VERSION)
* Sidekiq image version which is a [value in the chart](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/blob/18a3efa89bce59bf2f3c6d28d37cdcd9ade7164f/pre.yaml#L20)
* Or as an override in [k8s-workloads/gitlab-com]

There is no connection between specifying a version inside of our
[k8s-workloads/gitlab-com] project and what version our [CNG] project builds.
Due to this, we must ensure if we decide to deploy a custom version of the
Sidekiq image, that it already exists.  Otherwise the deploy will fail.  Issue:
https://gitlab.com/gitlab-com/gl-infra/delivery/issues/388 has been created to
address this problem.

This deployment model is not currently adapted to our auto-deployment model.
See https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/125 for details on
this implementation.

### Patching

Currently we utilize
[patcher](https://ops.gitlab.net/gitlab-com/gl-infra/patcher) to hot patch code
running on GitLab.com if there are high severity issues or security threats that
need to be mitigated immediately.  This will no longer work as we migrate into
Kubernetes.  There does not exist any shortcut to make the procedure faster
either.  Issue to address:
https://gitlab.com/gitlab-com/gl-infra/delivery/issues/641

## Dogfooding

Wherever possible we try to leverage the GitLab application features for
managing the deployments on Kubernetes. This includes:

* AutoDevops
* CI pipelines
* Package registry

These issues are tracked in the [Dogfooding Kubernetes features issue
board](https://gitlab.com/groups/gitlab-org/-/boards/1284732?label_name[]=Delivery&label_name[]=Dogfooding&label_name[]=group%3A%3Aautodevops%20and%20kubernetes).

## Observability and Monitoring

The Sidekiq service is monitored using Prometheus and is installed in the cluster
in its own namespace using the [prometheus operator helm
chart](https://github.com/helm/charts/tree/master/stable/prometheus-operator)

Our existing dashboards for Sidekiq have been supplemented with additional
dashboards and charts have been updated to ensure we incorporate details of our
Kubernetes deployments.  These dashboards can be found here:
https://dashboards.gitlab.net/dashboards/f/sidekiq/sidekiq-service

The Sidekiq service runs its own exporter exposing metrics for monitoring the
performance and state of this service.  Using this exporter all existing rules
for prometheus are able to be utilized.  Minor tweaks to both rules and charts
have been completed to ensure we do not lose visibility as this service
transitions into Kubernetes.

We also have existing alerts provided by our Kubernetes Prometheus Operator
integration that we'll continue to utilize to monitor the health of the running
Pods in our infrastructure:
https://gitlab.com/gitlab-com/runbooks/blob/master/rules/kubernetes.yml#L28-73
  * If a Replicaset does not have the desired amount of Pods running
  * If Pods are not ready
  * If a Pod is stuck in a CrashLoop
  * If the Deployment does not have the desired amount of Pods running

Refer to any [queue specific file] for any further details outside of the above.

## Responsibility

* Subject Matter Experts:

  * Kubernetes: [Infrastructure Team](https://about.gitlab.com/handbook/engineering/infrastructure/team/)

## Testing

Testing to validate the usability of Sidekiq inside of Kubernetes was performed
on the preproduction environment.

## Logging

Currently logging in Sidekiq is tricky.  All logging coming from Sidekiq is
tossed into a single index.  This includes both json formatted output, and plain
text format, along with formats from other parts of the rails application.   See
issue https://gitlab.com/gitlab-org/charts/gitlab/issues/1757 for additional
details and desired improvement areas.

All logging for the GKE cluster and all of its service is handled by
StackDriver with a log sink to pubsub. Like with the existing infrastructure,
a pubsubbeat is used to consume logs from pubsub and forward them to
elasticsearch.

Logs can be found in Kibana:

* [Production Logs](https://log.gitlab.net/goto/dc4f46784d2eb03b28e52b7b9e0a8594)
* [Staging Logs](https://nonprod-log.gitlab.net/goto/f23b8ddedfdc5eacf3e86fa3c00be5de)

There are no changes to the implementation of logging with Sidekiq.  This means
that all logs for sidekiq instances will no longer go to our existing sidekiq
index.  You can follow our existing runbook for guides on retrieving logs: https://gitlab.com/gitlab-com/runbooks/blob/master/troubleshooting/kubernetes.md#kubernetes-log-hunting

And issue exists to discuss how to better this in the future:
https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/8948

[chef repo]: https://ops.gitlab.net/gitlab-cookbooks/chef-repo
[CNG]: https://gitlab.com/gitlab-org/build/CNG
[gkms vaults]: https://ops.gitlab.net/gitlab-cookbooks/chef-repo/blob/master/doc/use-chef-vaults.md
[k8s-workloads/gitlab-com]: https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com
[mail_room]: https://github.com/tpitale/mail_room
[queue specific file]: https://gitlab.com/gitlab-com/gl-infra/readiness/tree/master/sidekiq
[reliable fetcher]: https://gitlab.com/gitlab-org/Sidekiq-reliable-fetch/#gitlab-Sidekiq-fetcher
[Service Desk]: https://docs.gitlab.com/ee/user/project/service_desk.html
