# Container Registry CDN

## Summary

So far, the container registry redirects all `GET`/`HEAD` requests for blobs to Google Cloud Storage (GCS) using pre-signed URLs. The Package team now added a feature that allows redirecting these requests to Google Cloud CDN, using the GCS bucket as storage backend.
In order to save cost on network egress and to improve performance for data transfer, we will be enabling this feature on GitLab.com, using a [Google Cloud Load Balancer](https://cloud.google.com/load-balancing) with [Cloud CDN](https://cloud.google.com/cdn/) enabled in front of our existing Google Object Storage Bucket.

## Architecture

Once the Container Registry is configured to use the Cloud CDN, the request flow will be altered so that clients will be redirected to `cdn.registry.gitlab.com/<blob path>` instead of `storage.googleapis.com/<blob path>` with a signed URL.
See https://gitlab.com/gitlab-org/gitlab/-/issues/349417#request-flow for a detailed request flow.
Initially a [feature-flag](https://gitlab.com/gitlab-org/gitlab/-/issues/349717) will be available to send a percentage of request to the CDN so this feature can be slowly rolled out on Production, once the configuration is enabled.

Once we have set the feature flag so that 100% of requests are using the CDN redirect, the Container Registry service will provide a redirect URL to the CDN for all blob downloads.
These blobs are immutable so we have set a long cache expiration policy of 1 week for all objects.
There is no reason for us to ever invalidate cache manually. Pre-signed URLs for CDN will have a validity, which we will set to 15 minutes (to match the ones we generate for GCS). Additionally, all blob requests continue to flow through the container registry, which is responsible for ensuring that users have access to the desired blob before generating the URL and redirecting clients. Cache expiration is only set to make sure that any deleted blobs by the online garbage collector will eventually be removed from the CDN cache.

Note that the Google Cloud CDN will always cache these blobs for 1 week, regardless of `Cache-control` headers which is the behavior for all signed URLs. See the [Google CDN documentation](https://cloud.google.com/cdn/docs/using-signed-urls) for details.

### Secrets

A new secret is needed for this feature, the CDN key that is used to generate signed URLs.
There is a unique key generated per environment (preprod, staging, production) and it is stored with other secrets in our GKMS vault.
For a more detailed summary of how this secret is stored and how it can be rotated see the [Container Registry CDN runbook](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/registry/cdn.md#secret-key-and-key-rotation).

## Operational Risk Assessment

The Google CDN is Google managed infrastructure, with the existing Google Storage bucket as a storage backend.
The [`container_registry_cdn_redirect`](https://gitlab.com/gitlab-org/gitlab/-/issues/349717) feature flag will allow us to slowly roll this out on Production, and monitor for any impact it has on Container Registry usage.

## Security/Compliance

There will be additional logging collected by [the CDN stored in StackDriver](https://cloud.google.com/cdn/docs/logging), these logs are subject to the default StackDriver retention of 30days.
Cached Registry data will be distributed globally, instead of being served from the US region. This is similar to the existing CloudFlare CDN we use for web traffic, for a full list of locations see the [CloudCDN Points of presence](https://cloud.google.com/cdn/docs/locations).
There is an [issue opened with legal/compliance](https://gitlab.com/gitlab-com/legal-and-compliance/-/issues/740) that will be resolved prior to enabling the feature.

## Performance

We expect performance to improve after enabling this feature since clients in regions outside of the US will be able to pull cached data instead of using the US regional storage bucket.
The performance improvement will be felt from the client pulling blobs from cache which is not something we can easily measure, https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/15004 will track possibly evaluating this improvement but this will be non-blocking for the rollout.

## Monitoring

Monitoring for the CDN was added to the [Storage Detail dashboard for the Container Registry](https://dashboards.gitlab.net/d/registry-storage/registry-storage-detail?from=now-1h&to=now&var-PROMETHEUS_DS=Global&var-environment=gprd&var-stage=main&orgId=1).
At this time we have not incorporated CDN metrics into our component level SLIs, this is being discussed in https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/14982 and currently we consider this non-blocking for the deployment.
