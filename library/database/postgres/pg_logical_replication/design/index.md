---
layout: markdown_page
title: "PostgreSQL Logical Replication - Design"
---

## On this page
{:.no_toc}

- TOC
{:toc}
## Resources

Epic: [gl-infra/255](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/255)


## Design

This document explains logical replication in PostgreSQL: benefits, limitations, and use cases. PostgreSQL logical replication is a native option to the database, and it is widely used by the community, thus providing ample support in case of any problems.


### What is logical replication? How does it work?


[Logical replication](https://www.postgresql.org/docs/11/logical-replication.html) is a method of replicating tables and database objects and the changes that happen on them, based upon their primary key or replication identity that we could define to replace the primary key. With logical replication, we apply the SQL changes from a primary host to the secondaries related,  different from what happens with physical replication ( streaming), which uses exact block addresses and byte-by-byte replication. 


The model of logical replication consists of a publisher and a subscriber model. The publisher will share a set or all the tables of tables from one database. The tables have to exist in the subscriber, having the same column definition. 


The subscriber relies on another database on a different host. The subscriber will register to a publisher to receive data and apply the changes. One or more subscribers can subscribe to one or more publication nodes. Using cascade replication is supported and will allow us to generate more complex configurations.

The standard procedure for logical replication will be :
Logical replication will copy all the data from the publisher database to a subscriber. 
Once we accomplish this step, the publisher’s changes are sent to the subscriber as they occur in real-time. 
The subscriber applies the data in the same order as the publisher to guarantee the transactional consistency.

#### Points about logical replication to explain in detail:

* We define the number of workers to be working in a subscription for the initial load. The variable max_logical_replication_workers  will describe how many workers will be used for the logical replication process, consuming replication slots. Those workers will use CPU-cores in the primary host wherein our database ecosystem has the highest load. Considering that each of these workers will be working in a table base, we can expect that the larger tables will require a more extended amount of time.  And by consequence, we will have more logical replication lag to apply.

* After the initial load, the logical replication will start applying the changes based on the  Log Sequence Number LSN representing the sequential changes that are in the WAL files  ( [write-ahead log](https://www.postgresql.org/docs/11/wal-intro.html) ). Logical replication will use a change data capture CDC strategy, reading the WAL files from the publisher, and applying on the subscriber. This process is single-threaded. E.g., In case of executing an initial load optimized with streaming, it can take around 6 hours. Keep in mind the amount of WAL files generated in 24 hours could reach  2 TB of data to be applied. In that case, we could easily face a scenario to apply over 1 TB of data from the WAL logs. This hypothetical amount of WAL logs can take longer considering:

Due to how the CDC mechanism is implemented, the SQL statements are interpreted in a slower way. E.g., If we have an SQL statement as : 

`UPDATE set tax_value = 20 from cities where condition_field=1` ( e.g. and lets imagine will update 20.000 rows) . 

Logical replication will generate 20.000 updates by id like:

`UPDATE set tax_value = 20 from cities where id=4;` 

`UPDATE set tax_value = 20 from cities where id=5; …`

When we have tables without PK, we have to enable the REPLICATION IDENTITY to be FULL to avoid the PK creation. Still, the logical replication will generate an identifier based on all the columns from the table. The lack of PK will represent a higher penalization in any operation as UPDATE or DELETE since we would be executing a search in those tables without indexes. This type of search based on all the fields will be slower. 






### What are the limitations? 

Here is the list of limitations of the logical replication, for our version: 

Currently, logical replication is a brittle process that could break easily with changes like:
- Altering the table structure from the source from the replication.
- Executing a failover from the primary.
- Executing a truncate.

In the pre-requisites, all the tables need to have a primary key. In our current schema, we had some tables without a primary key.

PostgreSQL allows us to change the Identity replication level. In the case of tables without primary KEY, we need to set the REPLICATION IDENTITY to  FULL. The logical replication will analyze all the fields to find a unique value to compare and operate in the destination database. This situation’s main drawback is performance degradation since we are not using the index or primary key to find the rows updated or deleted at the subscriber.

To identify the tables without primary key and add the replication identity to full ( as we executed in the [issue](https://gitlab.com/gitlab-org/gitlab/-/issues/251072) ), as required for the logical replication, we used in our tests on staging the following command:
```
select 'ALTER TABLE '||tab.table_schema||'.'||tab.table_name||' REPLICA IDENTITY FULL ;'
from information_schema.tables tab
left join information_schema.table_constraints tco 
          on tab.table_schema = tco.table_schema
          and tab.table_name = tco.table_name 
          and tco.constraint_type = 'PRIMARY KEY'
where tab.table_type = 'BASE TABLE'
      and tab.table_schema not in ('pg_catalog', 'information_schema')
      and tco.constraint_name is null
order by tab.table_name;
```

the output was:
```
 ALTER TABLE public.analytics_language_trend_repository_languages REPLICA IDENTITY FULL ;
 ALTER TABLE public.approval_project_rules_protected_branches REPLICA IDENTITY FULL ;
 ALTER TABLE public.ci_build_trace_sections REPLICA IDENTITY FULL ;
 ALTER TABLE public.deployment_merge_requests REPLICA IDENTITY FULL ;
 ALTER TABLE public.elasticsearch_indexed_namespaces REPLICA IDENTITY FULL ;
 ALTER TABLE public.elasticsearch_indexed_projects REPLICA IDENTITY FULL ;
 ALTER TABLE public.issue_assignees REPLICA IDENTITY FULL ;
 ALTER TABLE public.issues_prometheus_alert_events REPLICA IDENTITY FULL ;
 ALTER TABLE public.issues_self_managed_prometheus_alert_events REPLICA IDENTITY FULL ;
 ALTER TABLE public.merge_request_context_commit_diff_files REPLICA IDENTITY FULL ;
 ALTER TABLE public.merge_request_diff_commits REPLICA IDENTITY FULL ;
 ALTER TABLE public.merge_request_diff_files REPLICA IDENTITY FULL ;
 ALTER TABLE public.milestone_releases REPLICA IDENTITY FULL ;
 ALTER TABLE public.my_table REPLICA IDENTITY FULL ;
 ALTER TABLE public.pg_stat_statements_history REPLICA IDENTITY FULL ;
 ALTER TABLE public.project_authorizations REPLICA IDENTITY FULL ;
 ALTER TABLE public.project_pages_metadata REPLICA IDENTITY FULL ;
 ALTER TABLE public.push_event_payloads REPLICA IDENTITY FULL ;
 ALTER TABLE public.repository_languages REPLICA IDENTITY FULL ;
 ALTER TABLE public.schema_migrations REPLICA IDENTITY FULL ;
 ALTER TABLE public.truncate_test REPLICA IDENTITY FULL ;
 ALTER TABLE public.user_interacted_projects REPLICA IDENTITY FULL ;
 ALTER TABLE public.users_security_dashboard_projects REPLICA IDENTITY FULL ;
(23 rows)
```




### What are the benefits?

The main use cases for logical replication could be : 

- Enable database checksums: Replicating the whole dataset to a new database and in this new database cluster, the checksums had already been enabled. Considering the time for the initial load, or strategies to disable indexes to load faster and enable them again, it would require a maintenance window that could exceed the low-peak time window. Also, to speed up the process, we would need to dedicate several CPU cores to the process that will impact our performance during peak times. We discard this possibility mainly due to the impact on the primary database load, and the fact that we could wait for days to catch up on the logical replication lag.


- Major version upgrade: We could execute a PostgreSQL upgrade near-zero downtime. I would like to get into the details of this proposal (at a later stage of this document).

- Databases for reporting: Creating Databases for reports or select the data we would like to share. A good use case could be to generate different subscriptions for Data warehousing and export the data needed, maintaining the PII restrictions. The main drawback would be that the replication is brittle and it could break in case of a failover, or any rollout of a new database schema (for example).

### In our real use case - GitLab.com, where could we make a difference given our SLA?

The best use case to use logical replication will be for a major release upgrade. We should keep in mind that the process can be brittle, and we would need a short period of a few hours to avoid any changes in the database schema.

As explained above, the logical replication is a single-threaded process.
 
We will replicate and create a new cluster, break the replication, and upgrade the database. The next step is applying this delta of data that is being generated on the old primary, from the moment that we broke the replication until the sync with the current traffic. We will use logical replication for it.
This delta should be the minimum possible, for this, we should execute this step at the lowest peak of activity on the primary database, to have fewer changes to apply.
Executing this change at the lowest peak time, we also reduce the number of connections and transactions in the database, to maintain our database ecosystem healthy. We will detail one of the next steps that we are pausing all the connections on the pgbouncer layer.  If we exceed the threshold, we could initialize the new database cluster under a high load, impacting our SLAs.

#### Major upgrade version 

We should follow these steps to prepare a major release upgrade with near-zero downtime.

- Create a new cluster from Patroni, replicating from the production cluster. This new cluster will be created based on stream replication from the current primary of the production cluster.

- Create the replication slot for logical replication. With this step, we are collecting all the events that will be replicated.

- Promote the new cluster to stop replicating by streaming. We execute this step since it is a prerequisite for the upgrade.

- On the new cluster, we need to verify in the PostgreSQL log, what was the last LSN applied. The LSN is a value used in the logical replication to indicate where to read the publisher’s changes and apply them to the subscriber.


- Execute the pg_upgrade on the new cluster in the primary and the secondaries nodes.

- On the new cluster, we need to collect statistics and gather the information for the VACUUM process. This step will ensure that the database will have the data dictionary optimized to receive traffic.

- Set the logical replication slot to the last applied LSN on the promoted database, so we will apply all the changes that happened since we broke the stream replication.

- Enable logical replication from the old primary to the new primary, applying all the changes from this time frame.

- Monitor the logical replication lag to verify if we are applying all the changes, and we are without logical replication lag to execute a switchover. We need to monitor the LSN generated on the low-peak time, verifying if we can proceed with this switchover in a few seconds. 

- Put all the PgBouncers in pause. With this, we would queue up all the transactions for a few seconds. The idea is during this time, we do not have connections to the database, and the latency from the applications will increase, but we will not stop the access to the database.

- We will apply the maximum sequence IDs on the primary of the new cluster. With this step, the sequences will be updated and will be able to keep inserting an auto-incremented values on the new primary database, avoiding data collision. We need to execute this step when there is no traffic on the old primary to avoid collision between the data that could be inserted by the logical replication and the further incoming traffic in the new primary database from the new cluster.

- Execute the switchover, changing the pgbouncer to connect to the new primary and restore the traffic.


#### Points to be investigated or measured more in detail : 

We need to evaluate how the traffic is at the low-peak time that we suggest, that will be crucial for the smooth execution of the project, considering:

* The analysis of how long pgbouncer can be in pause mode, without generating a saturation on this component. We need to understand the maximum of connections that can be paused, without losing any, and how many seconds this can represent. 

* Consider the impact of the restoring of the pgbouncer traffic, and if possible, how could we make it smoother during the restore.

* We need to evaluate how long it could take to apply all the changes during a low peak time. We could analyze the number of bytes from the LSN changes that need to be applied in the subscriber as an estimation. This action has to be synchronized during the time that we maintain the pgbouncers in a paused state.
